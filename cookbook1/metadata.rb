name        "cookbook1"
description "Gives write access to upload directory."
maintainer  "Munathara"
license     "Apache 2.0"
version     "1.0.0"

depends "deploy"
